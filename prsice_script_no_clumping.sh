#!/bin/bash

##########################

# Need to define some variables 

# set to false if you want to keep all of the prsice temporary files
CLEANUP=true 


# Where to find the R scripts  PRSice_v1.25_noplots.R  and barplot_fromprofiles.r  

Rscriptdir=/hpf/projects/arnold/users/mlemire/scripts/prsice

# Path and name for the base data set (summary stats).
# Must have a header at least the following columns 
# (order not important)

# for speed, only SNPs with P<=0.50 and imputation quality >= 0.80
# can be extracted 

# SNP CHR BP A1 A2 P BETA

summstat=

# path and name for the plink binary files (target data set) 
# withoug the .bed, .bim or .fam suffix 
# these should be genotype files after qc, and containing 
# only SNPs with high imputation quality (e.g. >=0.80)
# and samples that will enter the analysis 

plinkfiles=

# path and name of the phenotype file
# no header necessary
# PRSICE ONLY USED IID, NOT FID, SO THE COLUMNS ARE
# IID PHENO

phenofile=

# path and name of the covariate file
# A HEADER IS NEEDED!
# SAME, ONLY IID MUST BE FOUND, COLUMNS ARE, E.G.
# IID PC1 PC2 PC2 SEX AGE AGExSEX AGE2

# LEAVE EMPTY IF THERE ARE NO COVARIATES 

covfile=

# comma-delimited list of the covariates,
# as found in the header of the user.covariate.file
# e.g. PC1,PC2,PC2,SEX,AGE,AGExSEX,AGE2
#
# in order to include interaction between two covariates (e.g. AGExSEX), 
# the product between the two covariates must be done externally 
# and included as an additional covariate. E.g, age was multiplied 
# by sex and the result was included as a column named AGExSEX 
# in the user.covariate.file. Same for AGE^2 

covarlist=

# the location (if not in path already) and name for the plink executable
# (has been tested only using plink version 1.9)

plinkexec=/hpf/tools/centos6/plink/1.90b3x/plink

# name for output 

output=

# YLIM upper limit for the PRS graph (upper limit for the R2 value)

ylim=0.002 


###########################
#
#  Note that R must be available in the path.
# 


if [ ! -z $covfile ]; then 

R -q --file=$Rscriptdir/PRSice_v1.25_noplots.R \
   --args base $summstat   \
  target $plinkfiles \
  slower 0   supper 0.50   sinc 0.1 fastscore T \
  covary T  clump.snps F  \
  plink $plinkexec  \
  figname $output   \
  pheno.file $phenofile  \
  binary.target F  \
  user.covariate.file $covfile   \
  covariates $covarlist  \
  cleanup F 


# this is a script generated in-house to produce details about beta and se 
# of each prs, as well as figure. 

R --no-save --args ${output}_RAW_RESULTS_DATA.txt   \
 $phenofile $covfile $ylim  $output FALSE < $Rscriptdir/barplot_fromprofiles.r


else

R -q --file=$Rscriptdir/PRSice_v1.25_noplots.R \
   --args base $summstat   \
  target $plinkfiles \
  slower 0   supper 0.50   sinc 0.1 fastscore T \
  covary F  clump.snps F  \
  plink $plinkexec  \
  figname $output   \
  pheno.file $phenofile  \
  binary.target F  \
  cleanup F


# this is a script generated in-house to produce details about beta and se
# of each prs, as well as figure.

R --no-save --args ${output}_RAW_RESULTS_DATA.txt   \
 $phenofile NULL  $ylim  $output FALSE < $Rscriptdir/barplot_fromprofiles.r


fi 




if $CLEANUP; then 

\rm Complete_Allele_List.txt
\rm HEADER
\rm LE_SNPs
\rm PROFILES.nosex
\rm TARGET_SNPs
\rm base_SNPS
\rm cleaned_base
\rm cleaned_base.assoc
\rm cleaned_base.clumped
\rm cleaned_base.log
\rm cleaned_base.nosex
\rm flip_list.txt
\rm flipped_target.bed
\rm flipped_target.bim
\rm flipped_target.fam
\rm flipped_target.log
\rm flipped_target.nosex
\rm head_disc
\rm mhc.txt
\rm non_synonymous_snps_only.bed
\rm non_synonymous_snps_only.bim
\rm non_synonymous_snps_only.fam
\rm non_synonymous_snps_only.log
\rm non_synonymous_snps_only.nosex
\rm profile_list
\rm rangelist.txt
\rm rangelist_ranges
\rm rawfile.raw
\rm reordered_base
\rm synonymous_snps
\rm temp.raw

fi 

