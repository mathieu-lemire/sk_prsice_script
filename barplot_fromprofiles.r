
args<- commandArgs(T)

YLIM<- as.numeric( args[4] )
output<-args[5]
binary<-as.logical( args[6] )

raw<- read.table(args[1], head=T)

phen<- read.table(args[2] , header= F )
names( phen )<- c("IID","PHEN") 

if( file.exists(args[3])  ){

 cov<- read.table( args[3], header=T )
 mrg<-merge( phen, cov, by=1 )
 form<- as.formula( paste0( "PHEN ~ SCORE +", paste( names(cov)[-1], collapse="+") ) )

} else {
  mrg<-phen 
  form<- as.formula( "PHEN ~ SCORE"  ) 
}



betascore<-c()

profile.files<- system("ls PROFILES.*.profile", intern=T ) 


thresholds<- gsub( ".profile", "", gsub("PROFILES.","", profile.files ) )

sink(paste0(output,".linearmodels.out")  )
for( t in thresholds ){


 cat("##########  P-value threshold ", t, "###############\n") 
 pro<- read.table(paste0("PROFILES.",t,".profile"), head=T )
 mrg.pro <- merge( mrg, pro, by="IID" )
 # scaling SCORE to unit variance so beta is wrt 1 sd increase in prs

 mrg.pro$SCORE<- mrg.pro$SCORE/sd( mrg.pro$SCORE, na.rm=T )

 if( binary ){
   linm<- glm( form, data=mrg.pro , family="binomial" ) 
 } else {
   linm<- lm( form, data=mrg.pro  )
 }
 betascore<-c( betascore, coef( linm )[2]  )

 print( summary(linm ) )
}
sink()



if( is.na( YLIM ) ){ 
 YLIM<- c(0, max( raw$r2.out )*1.25 ) 
} else {
 YLIM=c(0,YLIM)
}

raw$print.p[round(raw$p.out, digits = 3) != 0] <- round(raw$p.out[round(raw$p.out, digits = 3) != 0], digits = 3)
raw$print.p[round(raw$p.out, digits = 3) == 0] <- format(raw$p.out[round(raw$p.out, digits = 3) == 0], digits=2)
raw$print.p <- sub("e", "*x*10^", raw$print.p)


pdf(paste0( output,".pdf")  )


col<- rep( gray(0.45), nrow( raw ) )
col<- rep( "#79a3e8" , nrow( raw ) )

col[ betascore < 0 ]<- "tomato" 
bp<-barplot( raw$r2.out,    
         xlab = expression(italic(P)[T]),
	 ylab = expression(R^2) , ylim=YLIM, col=col  )

axis( 1, at=bp[,1], labels=raw[,1] ) 

text( labels=parse(text=paste(raw$print.p)) , 
	        x = bp[,1] +0.1, 
	        y =  raw$r2.out, pos=3 )
#	        srt = 45)

dev.off()


############# Computing PRS_PCA DOI: 10.1002/gepi.22339 


files<- system("ls PROFILES.*.profile", intern=T )

f<-files[1]
prs<- read.table( f, head=T  )
prs<- prs[,c(1,2,6)]
names( prs )[3]<- paste0( "P",sub( ".profile","", sub("PROFILES.", "", f ) ) )
for( f in files[-1] ){
 tmp<- read.table( f, head=T  )[,6, drop=F ]
 names(tmp)<- paste0( "P",sub( ".profile","", sub("PROFILES.", "", f ) ) )
 prs<- cbind( prs, tmp )
}

pc<- prcomp( prs[,-(1:2)], center=T, scale.=T  )

pc1<- data.frame(IID=prs[,2], PRS_PC1=pc$x[,1] )

mrg <- merge( pc1, mrg , by="IID"  )
if(  file.exists(args[3])  ){

 fo.full <- as.formula( paste0( "PHEN ~ PRS_PC1  +", paste( names(cov)[-1], collapse="+") ) )
 fo.cov <- as.formula( paste0( "PHEN ~ ", paste( names(cov)[-1], collapse="+") ) )

} else {
 fo.full<- as.formula(  "PHEN ~ PRS_PC1"   )
 fo.cov<- as.formula(  "PHEN ~ 1" )
}

 lm.full<- summary(lm( fo.full , data=mrg ) )
 lm.cov<- summary(lm( fo.cov, data=mrg ))


sink(paste0(output,".prs_pca.out")  )


print( lm.full ) 

r2<- lm.full$r.squared-lm.cov$r.squared

cat( "R2 (PRS_PC1): ", r2,"\n")

# effective number of independent tests

M<- ncol(prs)-2
Meff<- 1+ (M-1)*(1-var( pc$sdev^2)/M)

cat( "Effective number of independent PRS (Meff): ", Meff,"\n") 

sink()


write.table( pc1, paste0(output,".pc1.out") , col=T, row=F, quote=F )






